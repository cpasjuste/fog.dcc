#!/bin/bash

set -xe

# messages
merror() {
  printf '\033[1;31mERROR:\033[0m %s\n' "$@" >&2  # bold red
}

minfo() {
  printf '\n\033[1;36m> %s\033[0m\n' "$@" >&2  # bold cyan
}

# cleanup on exit
function cleanup {
  if [ $? -ne 0 ]; then
    merror 'oups, something went wrong, exiting...'
  fi
}

# set exit trap
trap cleanup EXIT

main() {
  # let's go...
  minfo "fog.dcc: arch: ${ARCH}"
  
  # copy overlay files to mount root
  cp -r overlay/. /
  
  # update intramfs with dell cc hook
  /usr/bin/mkinitcpio -p linux
  
  # copy kernel and initrd to final directory
  mkdir -p ${OUTPUT_DIR}
  cp /boot/vmlinuz-linux ${OUTPUT_DIR}
  cp /boot/initramfs-linux.img ${OUTPUT_DIR}
  chmod -R 777 ${OUTPUT_DIR}
}

main "$@"

