#!/bin/bash

build() {
  local ARCH="x86_64"
  local DOCKER_IMG="archlinux/archlinux:base"
  echo "[build] arch: $ARCH, image: $DOCKER_IMG"
  
  # pull image
  docker pull ${DOCKER_IMG}
  
  # install target packages in docker image for pacstrap package caching
  # this also populate PACKAGES variable
  source configs/packages

  # build image with packages as argument
  docker build \
    --build-arg DOCKER_IMG="${DOCKER_IMG}" \
    -t fog.dcc-${ARCH} \
    .

  # let's go !
  docker run --rm --privileged=true \
    -v /dev:/dev:ro \
    -v $(pwd)/output:/output \
    -e ARCH=${ARCH} \
    fog.dcc-${ARCH}
}

run() {
  # TODO
  qemu-system-x86_64 -m 1G -smp 4 \
    -kernel output/vmlinuz-linux \
    -initrd output/initramfs-linux.img \
    -append "setuppwd=SetupPwdTest valsetuppwd=ValPwdTest" \
    -serial stdio
}

show_usage() {
  echo "usage: $(basename "$0") [-b] [-r]"
  echo ""
  echo "examples:"
  echo "       $(basename "$0") -b | build x86_64 arch platform"
  echo "       $(basename "$0") -r | run with qemu"
}

main() {
  # parse args
  #test $# -eq 0 && set -- "-h"
  while getopts "brh" ARG; do
    case "$ARG" in
      b) build; return 0;;
      r) run; return 0;;
      *) show_usage; return 1;;
    esac
  done
}

main "$@"

