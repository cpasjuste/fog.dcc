# fog.dcc

**<ins>Building on ubuntu</ins>**
- Install build dependencies:
    ```
    sudo apt -yq update
    sudo apt -yq install docker.io
    ```
- Fix docker permissions (reboot for changes to take effect):
    ```
    sudo groupadd docker
    sudo usermod -aG docker ${USER}
    ```
- Build custom kernel and initramfs with Dell Command | Configure:
    ```
    ./dcc.sh -b
    ```
- Run image (testing):
    ```
    ./dcc.sh -r
    ```
    
 - Add FOG iPXE menu entry with this settings:
    ```
    kernel http://${​​​​fog-ip}​​​​​​​​​​​/iso/dcc/vmlinuz-linux
    initrd http://${​​​​​​​​​​​fog-ip}​​​​​​​​​​​​​​​​​​/iso/dcc/initramfs-linux.img
    boot || goto failed
    goto start
    ```
- Define optional bios passwords:
    ```
    imgargs vmlinuz-linux setuppwd=SetupPwdTest valsetuppwd=ValPwdTest
    ```
