ARG DOCKER_IMG="archlinux/archlinux:base"
FROM ${DOCKER_IMG}
LABEL contributor="david.carre@xxx.xx"

# update and install build requirements
RUN pacman -Syyu --noconfirm
RUN pacman -S --needed --noconfirm base linux amd-ucode intel-ucode mkinitcpio openssl-1.1

# add repo to build directory
ADD . /build

# set build environnement
ENV OUTPUT_DIR /output
WORKDIR /build

# let's go !
ENTRYPOINT ["/build/dockerbuild.sh"]

